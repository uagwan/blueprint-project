from scipy.optimize import minimize
import pandas as pd 
import numpy as np 
import time

starttime = time.time()
print("Loading model data")

# Load model data. To modify parameter values, edit the CSV file parameters.csv
params = pd.read_csv("parameters.csv")
params = params["Value"].tolist()
charge_cap = float(params[0]) #Max charge power flow into battery
discharge_cap = float(params[1]) # Max discharge power flow from battery
eta = float(params[3]) #Roundtrip efficiency of battery
discharge_energy_cap = float(params[2]) # Max discharge capacity of battery (kWh)
daily_discharge_cap = float(params[4]) # Max discharge throughput per day (kWh) <=> number of cycles permitted per day * capacity
price_filepath = params[5] #CSV file from where prices are to be sourced
zone = params[6] #zone within NYISO

# Load price data
pricedf = pd.read_csv(price_filepath, usecols=[0,1,3], index_col=1, parse_dates=True).loc[zone]
price = 10e-3*np.array(pricedf["LBMP ($/MWHr)"].tolist()) #changing price to $/kWh

# Check time horizon of optimization
timehorizon = len(price)

############################################
# A note on battery operation:
# 1) The variable over which optimization occurs is battery operation (represented by x)
#   x is a 24-dimensional vector, comprising of energy charge/discharge at each hour of the day 
# 2) Charging: x[i] > 0 ; Discharging: x[i] < 0. x[i] is energy drawn from/supplied to grid, and is not necessarily the energy flow into/out of battery
#   Only one of the two modes can be active at once
# 3) Roundtrip efficiency is completely accounted at the time of charging, which means the energy 'stored' in the battery can be completely discharged
#   When x[i] > 0, energy change in battery = eta*x[i]
#   When x[i] < 0, energy change in battery = x[i]
# 4) To get the positive energy flows, use x[i] + abs(x[i]). For negative flows, use x[i] - abs(x[i]). The abs() function is piecewise linear
#############################################
# A note on the optimization problem formulation:
# The optimization problem discussed below can be made into a linear program. To do so, we replace the optimisation variable with a 48-dimensional vector x
# y = x[0:24] -> the battery operation
# t = x[24:] -> |y|
# Charging at any hour = (y[i] + t[i])/2
# Discharging at any hour = (y[i] - t[i])/2
# This is done to select the positive and negative parts respectively
# The constraints and objective can all be formulated as linear functions of the variable x
# An additional constraint is added: t = |y| which is equivalent to t >= y, t >= -y
# The value of t does not blow up as it is otherwise constrained in other constraints, e.g. soc_max and soc_min
#############################################

print("Setting up optimization (objective + constraints)")
x0 = np.zeros(2*timehorizon) # Initializing

# The objective is to maximize revenue, which translates to minimizing costs. Cost[i] = p[i]*x[i]
# Cost[i] > 0 => energy drawn from grid
# Cost[i] < 0 => energy supplied to grid
def revobjective(x, price):
    y = x[0:24]
    t = x[24:]
    return sum(price*y)

# Constraint: charge in battery should not exceed the maximum battery capacity at any hour
def soc_max(x, discharge_energy_cap):
    y = x[0:24]
    t = x[24:]
    ltri_24 = np.tril(np.ones((24, 24)))
    soc = np.matmul(ltri_24,y)*(eta+1)/2 + np.matmul(ltri_24,t)*(eta-1)/2
    return discharge_energy_cap - soc
    
# Constraint: charge in battery should not be negative at any hour
def soc_min(x):
    y = x[0:24]
    t = x[24:]
    ltri_24 = np.tril(np.ones((24, 24)))
    soc = np.matmul(ltri_24,y)*(eta+1)/2 + np.matmul(ltri_24,t)*(eta-1)/2
    return soc

# Constraint: charging at any hour should not be more than the maximum charging rate 
# (since resolution is 1 hour, we can use the kW limit as a kWh limit assuming uniform power flow throughout the hour)
def fn_charge_cap(x, charge_cap):
    y = x[0:24]
    t = x[24:]
    return charge_cap - y

# Constraint: discharging at any hour should not be faster than maximum discharging rate
def fn_discharge_cap(x, discharge_cap):
    y = x[0:24]
    t = x[24:]
    return y + discharge_cap

# Constraint: the maximum energy discharged over the day should not exceed the daily discharge cap
def fn_daily_discharge_cap(x, daily_discharge_cap):
    y = x[0:24]
    t = x[24:]
    discharge = sum(t - y)/2
    return daily_discharge_cap - discharge

# Constraint: ensuring t >= y. Along with absx2, this ensures that t = |y|
def absx1(x):
    y = x[0:24]
    t = x[24:]
    return t - y

# Constraint: ensuring t >= -y. Along with absx1, this ensures that t = |y|
def absx2(x):
    y = x[0:24]
    t = x[24:]
    return t + y

# Setting up constraints
con_soc_max = {'type':'ineq', 'fun':soc_max, 'args':[discharge_energy_cap]}
con_soc_min = {'type':'ineq', 'fun':soc_min}
con_charge_cap = {'type':'ineq', 'fun':fn_charge_cap, 'args':[charge_cap]}
con_discharge_cap = {'type':'ineq', 'fun':fn_discharge_cap, 'args':[discharge_cap]}
con_daily_discharge_cap = {'type':'ineq', 'fun':fn_daily_discharge_cap, 'args':[daily_discharge_cap]}
con_absx1 = {'type':'ineq', 'fun':absx1}
con_absx2 = {'type':'ineq', 'fun':absx2}
cons = (con_soc_max, con_soc_min, con_charge_cap, con_discharge_cap, con_daily_discharge_cap, con_absx1, con_absx2)

print("Solving the optimization problem")
# Since objective is linear, set Hessian to zero. Then minimize objective
def fun_hess(x, price):
    return np.zeros((2*timehorizon,2*timehorizon))
sol = minimize(revobjective, x0, args=(price),hess=fun_hess, tol=10e-10,constraints=cons, options={'maxiter':10000}, method='trust-constr')

##################################################################
# Analyzing solution to optimization problem:

# Scipy minimize tends to violate bounds with small errors. Rounding off those errors,
err_tol = 10e-10 #tolerance below which we will set value to zero
def round_err(z, err_tol):
    return np.greater(abs(z), err_tol)*z + 0.

x = round_err(sol['x'], err_tol)
y = x[0:24]
t = x[24:]

print("###########################################################")
print("Optimizer exited successfully:" , sol['success'])

# Checking violation of constraints:
print("Checking whether constraints hold")
print("Constraint on maximum energy stored in battery (SOC) holds:",sum(round_err(soc_max(x, discharge_energy_cap), err_tol)>=0) == timehorizon)
print("Constraint on minimum SOC of battery holds:",sum(round_err(soc_min(x), err_tol)>=0)==timehorizon)
print("Constraint on charging rate holds:",sum(round_err(fn_charge_cap(x, charge_cap),err_tol)>=0) == timehorizon)
print("Constraint on discharging rate holds:",sum(round_err(fn_discharge_cap(x, discharge_cap), err_tol)>=0) == timehorizon)
print("Constraint on daily discharged energy holds:",fn_daily_discharge_cap(x, daily_discharge_cap)>=0)
print("Constraint on optimization vaiable (t = |y|) holds:",(sum(round_err(absx1(x), err_tol)>=0) == timehorizon) and (sum(round_err(absx2(x), err_tol)>=0) == timehorizon))
# print(sum(round_err(absx2(x), err_tol)>=0) == timehorizon)

print("###########################################################")

print("Optimal battery operation is ",y)

print("Revenue is", -revobjective(x, price))

# Storing outputs in a txt file
timestr = time.strftime("%Y%B%d-%H%M%S")
filename = "opt-output-"+timestr+".txt"
print("Further outputs stored in file", filename)

chargingcost = sum(price*(y+t))/2
discharge_over_day = sum(t-y)/2
hourly_revenue = -y*price
energy_flow = eta*(y + t)/2 + (y - t)/2


print("Total revenue generation for the day is ($)", sum(hourly_revenue), file=open(filename, "a"))
print("Total battery charging cost for the day is ($)", chargingcost, file=open(filename, "a"))
print("Total battery storage discharged during the day is (kWh)", discharge_over_day, file=open(filename, "a"))
print("Net energy change of battery for each hour is (kWh) (which is different from power available to the grid)", energy_flow, file=open(filename,"a"))
print("Net power available for use from battery/ power flow into battery at each hour is (kW)", y, file=open(filename,"a"))
print("LBMP is ($/kWh)", price, file=open(filename,"a"))
print("Revenue at each hour is ($)", hourly_revenue, file=open(filename,"a"))

endtime = time.time()
print("Time taken:", endtime - starttime)