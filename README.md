To run the model:
1) Update parameters as needed in parameters.csv
2) Run file run_this.py (In terminal, $ python run_this.py)
3) Results will be stored in "opt-output-%Y%B%d-%H%M%S.txt" (according to timestamp)

Dependencies:
1) Scipy ($ pip install scipy) version used: 1.3.0
2) Numpy ($ pip install numpy) version used: 1.17.0
3) Pandas ($ pip install pandas) version used: 0.23.4
4) time


Insights:
1) The solution mainly charges the battery during the three hours with the lowest prices, and then discharges at the hours with the highest prices
2) The problem can be formulated as a linear program, and though I used Scipy optimize, a general linear solver could be used
3) The optimization leads to small power flows at each hour. It might make sense to round them
4) Time taken is approximately 0.7s
